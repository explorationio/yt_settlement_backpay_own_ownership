import json
import os
import pickle
import httplib2
from oauth2client.file import Storage
from oauth2client.client import flow_from_clientsecrets
from oauth2client.tools import run_flow
from googleapiclient.discovery import build
from oauth2client.client import AccessTokenRefreshError
from googleapiclient.errors import HttpError
from apiclient.http import MediaIoBaseDownload
from io import FileIO

import time

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

class Youtube(object):
    def __init__(self):
        self.credential = None
        self.data = None
        self.reporting = None
        self.partner = None
        self.reporting = None
        self.content_owner = None
        self.cache = None
        self.daily_quota_reached = False
        self.logger = None

    @staticmethod
    def order_query(url):
        from urllib.parse import urlparse, parse_qs
        parsed = urlparse(url)
        qs = parse_qs(parsed.query.decode("utf-8"))
        new_qs = '?'
        for key in sorted(qs.keys()):
            value = qs.get(key)
            if isinstance(value, list):
                if isinstance(value[0], bytes):
                    value = value[0].decode("utf-8")
                else:
                    value = value[0]
            else:
                value = str(value)
            new_qs += key + '=' + value + '&'

        new_qs = new_qs.rstrip('&')
        ordered_query = parsed.scheme.decode('utf-8') + '://' + parsed.netloc.decode('utf-8') + parsed.path.decode(
            'utf-8') + new_qs + parsed.fragment.decode('utf-8')
        return ordered_query

    @staticmethod
    def build(secrets_file, scopes, content_owner, credentials_file_destination='oauth2.json'):
        message = """
        WARNING: Please configure OAuth 2.0

        To make this sample run you will need to populate the client_secrets.json file
        found at:

           %s

        with information from the Developers Console
        https://console.developers.google.com/

        For more information about the client_secrets.json file format, please visit:
        https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
        """ % os.path.abspath(os.path.join(os.path.dirname(__file__),
                                           secrets_file))
        flow = flow_from_clientsecrets(secrets_file,
                                       scope=scopes,
                                       message=message)

        storage = Storage(credentials_file_destination)
        credentials = storage.get()
        if credentials is None or credentials.invalid:
            credentials = run_flow(flow, storage)

        youtube = Youtube()
        youtube.data = build('youtube', 'v3', http=credentials.authorize(httplib2.Http()))
        youtube.partner = build('youtubePartner', 'v1', http=credentials.authorize(httplib2.Http()))
        youtube.reporting = build('youtubereporting', 'v1', http=credentials.authorize(httplib2.Http()))
        youtube.analytics = build('youtubeAnalytics', 'v1', http=credentials.authorize(httplib2.Http()))
        youtube.content_owner = content_owner
        return youtube

    def set_credential(self, credential):
        self.credential = credential

    def get_credential(self):
        return self.credential

    def get_data_service(self):
        return self.data

    def get_partner_service(self):
        return self.partner

    def get_analytics_service(self):
        return self.analytics

    def get_reporting_service(self):
        return self.reporting

    def execute(self, request, attempt=1):
        success = False
        retry = True
        error = None
        returned_result = None
        hash = self.order_query(request.uri.encode('utf-8'))
        if self.cache is not None and self.cache.get(hash) is not None:
            cached_response = pickle.loads(self.cache.get(hash))
            # print("%s found" % hash)
            return cached_response
        # print("%s not found" % hash)

        while not success and attempt <= 5 and retry:
            time.sleep((attempt - 1) * (attempt - 1))
            try:
                response = request.execute()
                success = True
            except AccessTokenRefreshError as e:
                raise e
            except HttpError as httpError:
                attempt += 1
                # print(httpError.content)
                json_error = json.loads(httpError.content.decode("utf-8"))
                if 'errors' in json_error['error'] and len(json_error['error']['errors']) > 0 and 'reason' in json_error['error']['errors'][0] and json_error['error']['errors'][0]['reason'] == 'dailyLimitExceeded':
                    self.daily_quota_reached = True
                    retry = False
                # print(httpError.resp)
                print('trying attempt %s on request %s' % (attempt, request.uri))
                error = httpError
                if httpError.resp.status != 403 and httpError.resp.status != 503:  # user rate exception or Backend error exception
                    retry = False

        if not success:
            if error.resp.status == 404:
                pass
            else:
                raise error
        else:
            returned_result = response

        if self.cache is not None:
            self.cache.set(hash, pickle.dumps(returned_result))
        return returned_result

    def search_by_isrc(self, isrc):
        if type(isrc) is list:
            response = self.search_asset(dict(isrcs=",".join(isrc)))
            return response
        response = self.search_asset(dict(isrcs=isrc))
        for item in response.get('items', []):
            if item.get('type', '') == 'sound_recording' and item.get('isrc','') in isrc:
                response = self.get_asset_details(item.get('id'))
                return response
        return None

    def get_asset_details(self, asset_id, metadata='effective', **kwargs):
        if kwargs.get('on_behalf_of_content_owner') is None:
            on_behalf_of_content_owner = self.content_owner
        else:
            on_behalf_of_content_owner = kwargs.get('on_behalf_of_content_owner')
        params = {
            'fetchMetadata': metadata,
            'fetchOwnership': kwargs.get('fetchOwnership','effective'),
            'onBehalfOfContentOwner': on_behalf_of_content_owner
        }

        if ',' in asset_id:
            asset_id = asset_id.split(',')

        asset_id_with_comma = asset_id
        if type(asset_id) is list:
            asset_id_with_comma = ",".join(asset_id)

        if ',' in asset_id_with_comma:
            method = 'list'
            params['id'] = asset_id_with_comma
        else:
            method = 'get'
            params['assetId'] = asset_id_with_comma

        if method == 'list' and len(asset_id) > 50:
            responses = []
            for i in range(0, len(asset_id), 49):
                params['id'] = ",".join(asset_id[i:i + 49])
                responses.append(self.execute(getattr(self.get_partner_service().assets(), method)(**params)))
            return responses
        else:
            return self.execute(getattr(self.get_partner_service().assets(), method)(**params))

    def search_video(self, title):
        params = {
            'q': title,
            'part': 'snippet'
        }
        return self.execute(getattr(self.get_data_service().search(), 'list')(**params))

    def claim_list(self, asset_id):
        return self.fetch_paged(self.get_partner_service().claimSearch(), 'list',dict(
            assetId=asset_id,
            onBehalfOfContentOwner=self.content_owner
        ))

    def get_content_owners_details(self, content_owners):
        return self.execute(self.get_partner_service().contentOwners().list(
            id=",".join(content_owners),
            onBehalfOfContentOwner=self.content_owner
        ))

    def search_asset(self, search_params={}):
        params = {
            'onBehalfOfContentOwner': self.content_owner,
            'ownershipRestriction': 'none'
        }
        params.update(search_params)
        return self.execute(self.get_partner_service().assetSearch().list(**params))

    def search_asset_paged(self, search_params=dict(), items_to_fetch=100):
        params = {
            'onBehalfOfContentOwner': self.content_owner,
            'ownershipRestriction': 'none'
        }
        params.update(search_params)
        search_results = self.fetch_paged(self.get_partner_service().assetSearch(), 'list', params, items_to_fetch)
        return search_results

    def get_content_owners(self):
        return self.execute(self.get_partner_service().contentOwners().list(fetchMine=True))

    def fetch_paged(self, resource, method, params, items_to_fetch=0, **kwargs):
        results = []
        last_page = False
        page = 0
        while not last_page and (items_to_fetch == 0 or len(results) < items_to_fetch):
            page += 1
            response = self.execute(getattr(resource, method)(**params))
            next_page_token = response.get('nextPageToken') or False
            last_page = next_page_token is False
            if not last_page:
                params['pageToken'] = next_page_token
            results = results + response.get(kwargs.get('items_key','items'), [])
            if self.logger is not None:
                self.logger.info("Fetched page {page}, have {items} items".format(page=page, items=len(results)))
        return results

    def get_policy_by_name(self, name):
        """
        Returns policy identified by name. Content ID API has only a 'list' method that lists all policies. We return
        the policy with name matching required name
        Args:
            name (string): Policy name identifier

        Returns:
            Policy that was identified, None otherwise
        """
        response = self.execute(self.get_partner_service().policies().list(
            onBehalfOfContentOwner=self.content_owner
        ))
        if not 'items' in response:
            return None

        for policy in response.get('items',[]):
            if policy.get('name','') == name:
                return policy
        return None


    def get_asset_relationships(self, asset_id, **kwargs):
        """
            Returns a list of related assets for a given asset_id.
            Args:
                param1 (string) asset id to fetch relations for
                **kwargs available arguments are:
                                only_first_page (bool): fetch only first page of results
                                with_details (bool): for each related item, fetch additional details via Assets.get

            Returns:
                list: List of related asset Ids (https://developers.google.com/youtube/partner/docs/v1/assetRelationships#resource)
            """
        if kwargs.get('only_first_page',False):
            response = self.execute(self.get_partner_service().assetRelationships().list(
                assetId=asset_id,
                onBehalfOfContentOwner=self.content_owner
            ))
            related_items = response.get('items', [])
        else:
            related_items = self.fetch_paged(self.get_partner_service().assetRelationships(), 'list', {
                'assetId': asset_id,
                'onBehalfOfContentOwner': self.content_owner
            })

        related_ids = [{'id': v for k, v in x.items() if k == 'parentAssetId'} for x in related_items]

        if not kwargs.get('with_details', False) or len(related_items) == 0:
            return related_ids

        ids = [related_item.get('id') for related_item in related_ids]
        # because YT API allows to fetch more at the same time, we use this
        for page in chunks(ids,50):
            details = self.get_asset_details(page)
            if 'metadata' in details:
                # if there is only one item, then an object is returned instead of an array, we want to always
                # work with an array
                details = dict(items=[details])
            for detail in details.get('items', []):
                for related_id in related_ids:
                    if related_id.get('id') == detail.get('id'):
                        related_id['details'] = detail


        return related_ids

    def add_asset_label(self, label):
        """
        Creates a new asset label

        Args:
            label (string): New asset label

        Returns:
            True if the new asset label has been created, False otherwise

        Raises:
            HttpError
        """
        result = True
        try:
            self.execute(self.get_partner_service().assetLabels().insert(
                onBehalfOfContentOwner=self.content_owner,
                body=dict(labelName=label)
            ))
        except HttpError as e:
            if 'invalidLabelName' in e.content.decode('utf-8'):
                if self.logger is not None:
                    self.logger.error("Asset label '%s' has invalid name." % label)
                result = False
            else:
                raise
        return result

    def patch_custom_id(self, asset_id, custom_id):
        """
        Updates custom id of a given asset id
        Args:
            asset_id (string): Asset ID to update
            custom_id (string): New Custom ID

        Returns:
            True if update went successful, False otherwise
        """
        asset = dict(
            metadataMine=dict(
                customId=custom_id
            )
        )
        result = True
        try:
            self.execute(self.get_partner_service().assets().patch(
                onBehalfOfContentOwner=self.content_owner,
                body=asset,
                assetId=asset_id
            ))
        except HttpError as e:
            c = e.content.decode('utf-8')
            if "metadataMine.customId" in c:
                if self.logger is not None:
                    self.logger.error("Invalid Custom ID provided: %s" % custom_id)
                result = False
            else:
                raise
        return result

    def update_composition_asset_metadata_from_ingest(self, asset_id, **kwargs):
        """
        Updates a composition asset's metadata via the API
        Args:
            asset_id (string): Asset ID to update
            **kwargs: metadata to update. The field names must match those from the ingest sheet

        Returns: True if update went successful, False otherwise

        """
        asset = dict(
            metadataMine=dict(
                customId=kwargs.get('custom_id'),
                title=kwargs.get('title'),
                iswc=kwargs.get('iswc'),
                labels=kwargs.get('add_asset_labels'),
                hfa=kwargs.get('hfa_song_code'),
                writer=kwargs.get('writers')
            )
        )
        result = True
        try:
            self.execute(self.get_partner_service().assets().update(
                onBehalfOfContentOwner=self.content_owner,
                body=asset,
                assetId=asset_id
            ))
        except HttpError as e:
            c =  e.content.decode('utf-8')
            if 'metadataMine.iswc' in c:
                if self.logger is not None:
                    self.logger.error("Invalid ISWC provided: %s" % kwargs.get('iswc'))
                result = False
            elif 'metadataMine.hfa' in c:
                if self.logger is not None:
                    self.logger.error("Invalid HFA provided: %s" % kwargs.get('hfa_song_code'))
                result = False
            else:
                raise
        return result

    def get_ownership(self, asset_id):
        return self.execute(self.get_partner_service().ownership().get(
                onBehalfOfContentOwner=self.content_owner,
                assetId=asset_id
            ))

    def update_composition_asset_ownership_from_ingest(self, asset_id, **kwargs):
        """
        Updates an asset ownership. We're updating all details in one go, so every field has to be present in
        the incoming kwargs
        Args:
            asset_id (string): Asset ID to update
            **kwargs: Field names must match those from ingest sheet. Publisher is commented out
            because at the time of writing this, YT API Publisher resource was broken and I could not
            get any output from it.

        Returns:
            True if update went fine, False otherwise

        """
        ownership = dict(
            performance=('performance',[]),
            synchronization=('sync',[]),
            mechanical=('mechanical',[])
        )
        for type, tupl in ownership.items():
            data = dict(
                ratio=float(kwargs.get('%s_ownership_share' % tupl[0])),
                owner=kwargs.get('content_owner', self.content_owner),
                type=kwargs.get('%s_ownership_restriction' % tupl[0]),
                territories=kwargs.get('%s_ownership_territory' % tupl[0]),
                #publisher=kwargs.get('publisher_name')
            )
            tupl[1].append(data)
        result = True
        try:
            response = self.execute(self.get_partner_service().ownership().update(
                onBehalfOfContentOwner=self.content_owner,
                body={k:v[1] for k,v in ownership.items()},
                assetId=asset_id
            ))
            assert 'mechanical' in response
        except HttpError as e:
            c = e.content.decode('utf-8')
            if self.logger is not None:
                self.logger.error("Failed updating ownership: %s" % c)
            result = False
        return result

    def add_territory_to_ownership(self, asset_id, type, territory):
        """
        Adds a territory to an existing ownership. There is no API support for that in YT, so this gets the current
        asset ownership, adds the territory and sends it back
        Args:
            asset_id (string): Asset ID to update
            type (string): mechanical / performance / synchronization
            territory (list): list of territories to append. If such territory already exists in the
        Returns:
            True if update was successful, false otherwise
        """
        current_ownership = self.get_ownership(asset_id)
        assert type in current_ownership, "%s must be present in response" % type
        current_type = current_ownership[type][0]
        current_territories = set(current_type['territories'])
        current_type['territories'] = list(set(territory).union(current_territories))

        result = True
        try:
            response = self.execute(self.get_partner_service().ownership().update(
                onBehalfOfContentOwner=self.content_owner,
                body=current_ownership,
                assetId=asset_id
            ))
            assert 'mechanical' in response
        except HttpError as e:
            c = e.content.decode('utf-8')
            if self.logger is not None:
                self.logger.error("Failed adding ownership to territory: %s" % c)
            result = False
        return result



    def update_asset_from_ingest(self, asset_id, **kwargs):
        """
        Updates an asset via the API. The details to be updated are in kwargs. The names of parameters
        must match the names from the ingest sheet.

        Several API updates are part of this:
         1) Update any metadata via Assets.update
         2) Add relationships via AssetRelationships.insert
         4) Update ownership via Ownership.updated
         5) Update asset match policy via AssetMatchPolicy.update

        Args:
            asset_id (string): Asset to be updated

        Returns:
            True if asset has been updated correctly, False othetwise
        """
        pass


    def get_report_jobs(self):
        return self.execute(self.get_reporting_service().jobs().list(
            onBehalfOfContentOwner=self.content_owner,
            includeSystemManaged=True
        ))

    def get_reports(self, job_id):
        """
        Returns a list of available reports for a given job id
        Args:
            job_id (string): job id to return reports for

        Returns:

        """
        try:
            response = self.execute(self.get_reporting_service().jobs().reports().list(
                onBehalfOfContentOwner=self.content_owner,
                jobId=job_id
            ))
            if 'nextPageToken' in response:
                return self.fetch_paged(self.get_reporting_service().jobs().reports(),'list',dict(
                    onBehalfOfContentOwner=self.content_owner,
                    jobId=job_id
                ), items_key='reports')
        except HttpError as e:
            raise e
        return response

    def get_report_types(self):
        return self.execute(self.get_reporting_service().reportTypes().list(
            onBehalfOfContentOwner=self.content_owner,
            includeSystemManaged=True
        ))

    def download_report(self, report_url, destination):
        request = self.get_reporting_service().media().download(
            resourceName=destination
        )
        request.uri = report_url
        fh = FileIO(destination, mode='wb')
        # Stream/download the report in a single request.
        downloader = MediaIoBaseDownload(fh, request, chunksize=-1)

        done = False
        while done is False:
            status, done = downloader.next_chunk()
            if status:
                print("Download %d%%." % int(status.progress() * 100))
        print("Download Complete!")

    def get_asset_labels(self):
        response = self.execute(self.get_partner_service().assetLabels().list(
            onBehalfOfContentOwner=self.content_owner
        ))
        items = response.get('items',[])
        assert len(items) > 0, "There should always be some labels"
        return [x['labelName'] for x in items]

    def get_analytics_query(self, query):
        response = self.execute(self.get_analytics_service().reports().query(**query))
        return response



