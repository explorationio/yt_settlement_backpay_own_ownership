# README #

This script takes CSV files provided by Youtube for the Backpay Settlement Project. The output of this
script are 2 files.

* 'settlement_investigate.csv' - this file will contain lines which are not owned by any of your client

* 'settlement_ours.csv' - this file will contain lines where we already applied an ownership

This script expects an existing and valid CMS account, CMS API application