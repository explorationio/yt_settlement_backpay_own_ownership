import csv
from collections import OrderedDict

import redis
from yt_settlement_backpay_own_ownership.libs.youtube import Youtube, chunks

"""
This script takes CSV files provided by Youtube for the Backpay Settlement Project. The output of this
script are 2 files.
* 'settlement_investigate.csv' - this file will contain lines which are not owned by any of your client
* 'settlement_ours.csv' - this file will contain lines where we already applied an ownership

This script expects an existing and valid CMS account, CMS API application

"""

__author__ = "Michal Holub"
__copyright__ = "Exploration.io 2017"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Michal Holub"
__email__ = "michal.holub@exploration.io"
__status__ = "Production"

your_content_owner = ''
your_credentials_file = ''

# your credentials go here
youtube = Youtube.build(
    your_credentials_file,
    ["https://www.googleapis.com/auth/youtubepartner"],
    your_content_owner
)

# use redis for request caching
db = redis.StrictRedis(host='localhost', port=6379, db=2)
youtube.cache = db

# a dict of content owners you manage, key is name, value is ID
co = {

}

# for convenience, the list of content owners is inversed
co_i = {v: k for k, v in co.items()}
ownership_types = ['synchronization', 'mechanical', 'performance', 'lyric']

# final columns to write, those are the same ones that come in the settlement CSVs
columns = 'custom_id,asset_id,related_isrc,iswc,title,hfa_song_code,writers,match_policy,publisher_name,sync_ownership_share,sync_ownership_territory,sync_ownership_restriction'.split(
    ',')
page = 0

yt_settlement_path = '/home/mholub/www/exploration/==utils/backpay settlement March 2017/top100k.csv'

# populate our working array so we can effectively iterate over 50 asset IDs chunks
with open(yt_settlement_path) as csvfile:
    reader = csv.DictReader(csvfile)
    asset_ids = OrderedDict()
    for row in reader:
        asset_ids[row['asset_id']] = row

with open('settlement_investigate.csv', 'w') as csvresult, open('settlement_ours.csv', 'w') as csv_ours:
    writer = csv.DictWriter(csvresult, fieldnames=columns)
    writer_ours = csv.DictWriter(csv_ours, fieldnames=columns)
    writer.writeheader()
    writer_ours.writeheader()
    all_ids = list(asset_ids.keys())
    all_ids_len = len(all_ids)
    for chunk in chunks(all_ids, 50):
        print("Page {page}/{total}".format(page=page, total=int(all_ids_len / 50)), end='', flush=True)

        # get chunk details
        details = youtube.get_asset_details(chunk)
        print("...got YT details", end='', flush=True)

        for asset in details.get('items', []):
            asset_id = asset.get('id')
            if not asset_id in chunk:
                # it's possible that the asset that we're looking for comes up as an alias instead of an asset id
                for alias in asset.get('aliasId'):
                    if alias in chunk:
                        asset_id = alias
            if not asset_id in chunk:
                # if asset id is even not in alias, then we move on
                continue
            owners = dict()
            we_have_ownership = False
            for ot in ownership_types:
                for owner in asset.get('ownership', {}).get(ot, []):
                    if owner.get('owner') in co_i:
                        we_have_ownership = True

            if we_have_ownership:
                writer_ours.writerow(asset_ids[asset_id])
            else:
                writer.writerow(asset_ids[asset_id])
        page += 1
        print("...written to file")
